// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyAe5vZ2CpGWLzrwhRsqpRiYakpIob5ZX3E",
  authDomain: "fernandezfoods-319.firebaseapp.com",
  databaseURL: "https://fernandezfoods-319.firebaseio.com",
  projectId: "fernandezfoods-319",
  storageBucket: "fernandezfoods-319.appspot.com",
  messagingSenderId: "623711606734",
  appId: "1:623711606734:web:facc3a147768d966627d9f"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
