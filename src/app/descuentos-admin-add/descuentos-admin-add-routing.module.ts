import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DescuentosAdminAddPage } from './descuentos-admin-add.page';

const routes: Routes = [
  {
    path: '',
    component: DescuentosAdminAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DescuentosAdminAddPageRoutingModule {}
