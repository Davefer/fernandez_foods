import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescuentosAdminAddPage } from './descuentos-admin-add.page';

describe('DescuentosAdminAddPage', () => {
  let component: DescuentosAdminAddPage;
  let fixture: ComponentFixture<DescuentosAdminAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescuentosAdminAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescuentosAdminAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
