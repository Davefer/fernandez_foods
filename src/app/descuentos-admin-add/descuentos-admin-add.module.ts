import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescuentosAdminAddPageRoutingModule } from './descuentos-admin-add-routing.module';

import { DescuentosAdminAddPage } from './descuentos-admin-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescuentosAdminAddPageRoutingModule
  ],
  declarations: [DescuentosAdminAddPage]
})
export class DescuentosAdminAddPageModule {}
