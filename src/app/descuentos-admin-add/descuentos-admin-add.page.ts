import { Component, OnInit } from '@angular/core';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-descuentos-admin-add',
  templateUrl: './descuentos-admin-add.page.html',
  styleUrls: ['./descuentos-admin-add.page.scss'],
})
export class DescuentosAdminAddPage implements OnInit {

  idoferta:string;
  nombreoferta:string;
  descoferta:string;
  precioferta:any;
  auxnum:number = 0;

  constructor(
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private alertcontroller:AlertController,
    private db:AngularFirestore,
    private storage:AngularFireStorage,
    private router:Router,
    private loadingCtrl:LoadingController,
  ) { }



  ngOnInit() {
  }


  addOferta(){
    
    let idn = this.idoferta.replace(/\s/g, '');
    let nm = this.nombreoferta.replace(/\s/g, '');
    let ds = this.descoferta.replace(/\s/g, '');
  
    if(idn.length==0 || nm.length==0 || ds.length==0 || this.precioferta.length ==0){
      this.printalert("Necesitas introducir todos los datos");
    } else {

      let categoriesCollection:AngularFirestoreCollection = this.db.collection("ofertas");
      categoriesCollection.valueChanges().subscribe(
  
        res => {
          this.auxnum = 0;
          res.forEach(element => {

            if(element.id == this.idoferta){
              this.auxnum = 1;
            
            } 
          });


          this.paso2();
        })
    }

  }


  paso2(){
    if(this.auxnum == 1){
      this.printalert("Ya existe una oferta con esa ID")
      this.auxnum = 0;
    }else{
      this.auxnum = 0;

      

        this.loadingCtrl.dismiss();

          this.db.doc('/ofertas/' + this.idoferta).set({
            id: this.idoferta,
            nombre: this.nombreoferta,
            descripcion: this.descoferta,
            precio:this.precioferta
          })
          this.idoferta = "";
          this.nombreoferta ="";
          this.descoferta = "";
          this.precioferta = "";

          this.printalert("Oferta añadida")
          this.router.navigateByUrl('/tabs/ofertasadmin'); 


    }

  }



  back(){
    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/ofertasadmin');
  }

  async printalert(msg){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: msg,
      buttons: [{
          text: 'Aceptar',
          handler: () => {
  
          }}]});
  
    await alert.present();
  
    }
  
}
