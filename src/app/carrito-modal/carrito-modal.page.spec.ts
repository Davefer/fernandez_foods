import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CarritoModalPage } from './carrito-modal.page';

describe('CarritoModalPage', () => {
  let component: CarritoModalPage;
  let fixture: ComponentFixture<CarritoModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarritoModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CarritoModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
