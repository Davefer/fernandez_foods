import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito-modal',
  templateUrl: './carrito-modal.page.html',
  styleUrls: ['./carrito-modal.page.scss'],
})
export class CarritoModalPage implements OnInit {


  ishidden:boolean =true;
  condicion:string ="";

  ishidden2:boolean=true;
  condicion2:string ="";

  condicion3:string ="";

  desabilitado:boolean = true;

  totalprecio:number=0;
  preciocarrito:any;

  constructor(
    private modalCtrl:ModalController,
    private appservice:AppserviceService,
    private alertcontroller:AlertController,
    private paypal:PayPal,
    private router:Router,
      ) { }

  ngOnInit() {
  }

  localidad:string;
  calle:string;
  numero:any;
  piso:any;
  puerta:any;
  observaciones:any;
  usuario:any;
  hora:any;
  minuto:any;
  momento:any;
  horasel:any
  horanueva:any;


  ionViewWillEnter(){
    this.horanueva ="";
    this.observaciones ="";
    this.horasel = [];
    this.preciocarrito=[];

    this.preciocarrito = JSON.parse(localStorage.getItem("carrito"));
    this.usuario = JSON.parse(localStorage.getItem("user"));

    this.localidad = this.usuario.direccion.localidad
    this.calle = this.usuario.direccion.calle;
    this.numero = this.usuario.direccion.numero;
    this.piso = this.usuario.direccion.piso;
    this.puerta = this.usuario.direccion.puerta;

    let d = new Date();
    this.hora = d.getHours();
    this.minuto= d.getMinutes();
    this.minuto += 40;

    if(this.hora < 11){
      this.hora = 11
      this.minuto = 40
    
      this.momento = this.hora + ":" + this.minuto
    }else {
      if(this.minuto >= 60){
        this.hora += 1;
        this.minuto = this.minuto - 60;
  
        if(this.minuto < 10){
          this.minuto = "0"+this.minuto
        }
  
      } else {
        if(this.minuto < 10){
          this.minuto = "0"+this.minuto
        }
      }
      this.momento = this.hora + ":" + this.minuto
    }

    this.calculartotal()

  }

  calculartotal(){
    
    this.totalprecio = 0;
    this.preciocarrito.forEach(element => {
    this.totalprecio += parseInt(element.precio)

    });
    console.log(this.totalprecio);
  }


  radioGroupChange(ev){
    console.log(ev.detail.value);

    if(ev.detail.value == "domicilio"){
      this.ishidden = false
      this.condicion = "Envio a Domicilio"
    } else{
      this.ishidden = true
      this.condicion = "Recogida en Local"
      
    }

    if(this.condicion2 !="" && this.condicion!="" && this.condicion3!=""){
      this.desabilitado = false
    }

  }

  marcarPosponer(evt){

    this.horasel = [];

    if(evt.detail.value == "pos"){
      this.condicion2 = "Posponer";
      this.ishidden2 = false;

      
      let d = new Date();
    let auxh = d.getHours();
    let auxm = d.getMinutes();
        auxm +=40;

        if(auxh < 11){
          auxh = 11
          auxm = 40
        }
    let totaux = auxh+":"+auxm;

    console.log(auxh);
    console.log(auxm);
    console.log(totaux);
        
      for(let i=0;i<100;i++){

        auxm +=20;

        if(auxh <15){

          if(auxm <60){
            console.log(auxh+":"+auxm);
            let aux2 = auxh+":"+auxm;
            this.horasel.push(aux2);
          }else{
            if(auxm > 60 ){
              auxm = auxm - 60;
              auxh +=1;
  
              if(auxm <10){
                let newm = "0"+auxm;
                let aux2 = auxh+":"+newm;
                this.horasel.push(aux2);
              }else{
                let aux2 = auxh+":"+auxm;
                this.horasel.push(aux2);
              }
            }
  
            if(auxm ==60){
              auxm = 0;
              auxh += 1;
              let aux2 = auxh+":"+"00";
              this.horasel.push(aux2);
            }
          }

        }


          
      }
      this.horasel.splice(this.horasel.indexOf(this.horasel.last),1);
      console.log(this.horasel);

    } else {
      this.condicion2 = "No posponer";
      this.ishidden2 = true;
    }


    if(this.condicion2 !="" && this.condicion!="" && this.condicion3!=""){
      this.desabilitado = false
    }

  }

  marcarpago(evts){

    if(evts.detail.value == "efec"){
      this.condicion3 = "efectivo"
    }else{
      this.condicion3 = "tarjeta"
    }

    if(this.condicion2 !="" && this.condicion!="" && this.condicion3!=""){
      this.desabilitado = false
    }

  }

  nuevahora(evnt) {
    console.log(evnt.detail.value)
    this.horanueva = evnt.detail.value;
  }


  currency:string='EUR';
  currencyIcon:string='€';

 realizarCompra(){

  if(this.condicion3 =="tarjeta"){
    
    this.paypal.init({
      PayPalEnvironmentProduction:'ATBO2B3GOW13ahXPNguEFCyUA9W7nD_uT7ORZU7e5xo4NMTEoH6iP9X9huD5H87puB205ntlb6CD-qdr',
      PayPalEnvironmentSandbox:'ATBO2B3GOW13ahXPNguEFCyUA9W7nD_uT7ORZU7e5xo4NMTEoH6iP9X9huD5H87puB205ntlb6CD-qdr',
    }).then(()=>{

      this.paypal.prepareToRender('PayPalEnvironmentSandbox',new PayPalConfiguration({

      })).then(()=>{
        let payment = new PayPalPayment(this.totalprecio.toString(),this.currency,'Pago productos FernandezFoods','sale');
        this.paypal.renderSinglePaymentUI(payment).then((res)=>{
        

          let lo = this.localidad.replace(/\s/g, '');
          let ca = this.calle.replace(/\s/g, '');
    
          if(this.condicion2 =="Posponer") {
    
            if(lo.length == 0 || ca.length == 0 || this.numero.length == 0 || this.piso.length == 0 || this.puerta.length == 0 || this.horanueva.length == 0){
              this.printalert();
            }else{
                let callefin = this.calle+" "+this.numero+"-"+this.piso+"-"+this.puerta
                this.appservice.realizarCompra(callefin,this.condicion,this.condicion2,this.condicion3,this.horanueva,this.observaciones,this.localidad);
                this.modalCtrl.dismiss();
            }
    
          } else {
    
            if(lo.length == 0 || ca.length == 0 || this.numero.length == 0 || this.piso.length == 0 || this.puerta.length == 0 ){
              this.printalert();
            }else{
                let callefin = this.calle+" "+this.numero+"-"+this.piso+"-"+this.puerta
                this.appservice.realizarCompra(callefin,this.condicion,this.condicion2,this.condicion3,this.horanueva,this.observaciones,this.localidad);
                this.modalCtrl.dismiss();
            }
    
    
          }

        })
      })

    })

  } else {

    let lo = this.localidad.replace(/\s/g, '');
    let ca = this.calle.replace(/\s/g, '');

    if(this.condicion2 =="Posponer") {

      if(lo.length == 0 || ca.length == 0 || this.numero.length == 0 || this.piso.length == 0 || this.puerta.length == 0 || this.horanueva.length == 0){
        this.printalert();
      }else{
          let callefin = this.calle+" "+this.numero+"-"+this.piso+"-"+this.puerta
          this.appservice.realizarCompra(callefin,this.condicion,this.condicion2,this.condicion3,this.horanueva,this.observaciones,this.localidad);
          this.modalCtrl.dismiss();
      }

    } else {

      if(lo.length == 0 || ca.length == 0 || this.numero.length == 0 || this.piso.length == 0 || this.puerta.length == 0 ){
        this.printalert();
      }else{
          let callefin = this.calle+" "+this.numero+"-"+this.piso+"-"+this.puerta
          this.appservice.realizarCompra(callefin,this.condicion,this.condicion2,this.condicion3,this.horanueva,this.observaciones,this.localidad);
          this.modalCtrl.dismiss();
          
      } 

    }

  }


  }



  back(){
    this.modalCtrl.dismiss();
  }


  async printalert(){
    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'Faltan campos por rellenar',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {

          }
        }
      ]
    });
  
    await alert.present();
  }

}
