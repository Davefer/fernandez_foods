import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DescuentosAdminEditPage } from './descuentos-admin-edit.page';

const routes: Routes = [
  {
    path: '',
    component: DescuentosAdminEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DescuentosAdminEditPageRoutingModule {}
