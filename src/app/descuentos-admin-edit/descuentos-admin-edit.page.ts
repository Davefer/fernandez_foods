import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AppserviceService } from '../services/appservice.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-descuentos-admin-edit',
  templateUrl: './descuentos-admin-edit.page.html',
  styleUrls: ['./descuentos-admin-edit.page.scss'],
})
export class DescuentosAdminEditPage implements OnInit {

  idof:any
  nombreof:any;
  descof:any;
  preciof:any;

  constructor(
    private db:AngularFirestore,
    private appservice:AppserviceService,
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private alertcontroller:AlertController,
    private router:Router,
    ) { }

  ngOnInit() {
  }

ionViewWillEnter(){
  let categoriesCollection:AngularFirestoreCollection = this.db.collection("ofertas");
  categoriesCollection.valueChanges().subscribe(

    res => {

      res.forEach(element => {

        if(element.id == this.appservice.descuentoID){

          if(element.precio.toString().length == 4){
            element.precio = element.precio+"0";
            
          }
    
          if(element.precio.toString().length == 2 || element.precio.toString().length == 1){
            element.precio = element.precio+".00";
            console.log(element.precio);
          }
          
          if(element.precio.toString().length == 3 ){
            element.precio =  element.precio+"0";
           
          }

          this.idof = element.id;
          this.nombreof = element.nombre;
          this.descof = element.descripcion;
          this.preciof = element.precio;



        }

        

      });


      
    })
}

back(){
  let options : NativeTransitionOptions = {
    direction: 'right',
    duration:200,
    slowdownfactor:-1,
    iosdelay:50
  }

  this.pageTransition.slide(options);
  this.navCtrl.navigateRoot('/tabs/ofertasadmin');
}

async printalert(msg){

  const alert = await this.alertcontroller.create({
    header: '¡Alerta!',
    message: msg,
    buttons: [{
        text: 'Aceptar',
        handler: () => {

        }}]});

  await alert.present();

  }

  editOferta(){

    let nm = this.nombreof.replace(/\s/g, '');
    let ds = this.descof.replace(/\s/g, '');
    
    if(nm.length == 0 || ds.length == 0 || this.preciof.length == 0){
      this.printalert("Hay campos sin rellenar");
    } else{
      
      this.db.doc('/ofertas/'+this.idof).update({
        nombre:this.nombreof,
        descripcion:this.descof,
        precio:this.preciof,
      })
      this.idof = "";
      this.nombreof = "";
      this.descof = "";
      this.preciof = "";

      this.printalert("Oferta editada");
      this.router.navigateByUrl('/tabs/ofertasadmin')
    }

  }



}
