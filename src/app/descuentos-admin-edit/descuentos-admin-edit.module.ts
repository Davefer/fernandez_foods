import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescuentosAdminEditPageRoutingModule } from './descuentos-admin-edit-routing.module';

import { DescuentosAdminEditPage } from './descuentos-admin-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescuentosAdminEditPageRoutingModule
  ],
  declarations: [DescuentosAdminEditPage]
})
export class DescuentosAdminEditPageModule {}
