import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescuentosAdminEditPage } from './descuentos-admin-edit.page';

describe('DescuentosAdminEditPage', () => {
  let component: DescuentosAdminEditPage;
  let fixture: ComponentFixture<DescuentosAdminEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescuentosAdminEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescuentosAdminEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
