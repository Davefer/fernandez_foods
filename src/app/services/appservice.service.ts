import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';



@Injectable({
  providedIn: 'root'
})
export class AppserviceService {

  pedidoID:string;
  categoryID:string;
  productID:string;
  descuentoID:string;
  
  cantidadprods:number = 0;
  userType:boolean;
  blob:number = 0;


  constructor(
    private toastController:ToastController,
    private db:AngularFirestore, 
    private afAuth:AngularFireAuth,
    private router:Router,
    private loadingCtrl:LoadingController,
    private alertcontroller:AlertController,
  ) { }

  // Toast //

  async presentToast(errormens) {
   const toast = await this.toastController.create({
     message: errormens,
     duration: 1250,
     mode:'md'
   });
   toast.present();
 }

 async presentToast2(errormens) {
  const toast = await this.toastController.create({
    message: errormens,
    duration: 1250,
    mode:'md',
    position:'top'
  });
  toast.present();
}


 // -------------- //


// Creacion Usuario //

  userCreate(User,passw:string){

    return this.afAuth.auth.createUserWithEmailAndPassword(User.correo,passw)
    .then((newCredential:firebase.auth.UserCredential) =>
      {

       let userid = this.db.createId();
       
          User.id = userid;

        this.db.doc('/users/'+ User.id).set({
          id: User.id,
          nombre:User.nombre,
          apellidos:User.apellidos,
          correo:User.correo,
          tipo:User.tipo,
          direccion:{
            localidad:"",
            calle:"",
            numero:"",
            piso:"",
            puerta:""

          },

          telefono:User.telefono,
        });
        this.router.navigateByUrl("/home");
        this.presentToast("Usuario Creado");
      })
      
    .catch(error=>
      {
  
        if(error.message == "The email address is badly formatted."){
          this.presentToast("El formato del correo no es valido");
        }

        if(error.message == "Password should be at least 6 characters"){
          this.presentToast("La contraseña es demasiado débil");
        }
  
      });
  }
  // -------------------------------- //


  // Login usuario //

  userLogin(correo,passw){
   this.blob = 0;
    return this.afAuth.auth.signInWithEmailAndPassword(correo,passw)
      .then((newCredential:firebase.auth.UserCredential)=>
      {
        let usersCollection:AngularFirestoreCollection = this.db.collection("users");
          usersCollection.valueChanges().subscribe(

            res =>{

              res.forEach(element => {
                  if(element.correo == correo){
                    this.blob = 1;
                    localStorage.setItem("user",JSON.stringify(element));
                    setTimeout(()=>{
                      
                      if (element.tipo =="admin"){
                        this.userType = true;
                        this.router.navigateByUrl("tabs/pedidos");
             
                      }else{
                        this.userType = false;
                        this.router.navigateByUrl("tabs/info");
                        
                      }
                      console.log(element.tipo);
                      
                    },1000);

                    
                    
                  } 
                  
              })

              if(this.blob == 0){
                this.presentToast("Esta cuenta a sido eliminada");
              }

            }
          )

      })

      .catch(error=>
        {
          if(error.message.includes("There is no user record")){
            this.presentToast("El usuario no existe");
          }
          
          if(error.message.includes("The email address")){
            this.presentToast("Correo invalido");
          }

          if(error.message.includes("password")){
            this.presentToast("Contraseña incorrecta");
          }
          
        });
  
        


  }



  // -------------- //


  // Check Sesión iniciada //

    userSesion(){
      let userData = JSON.parse(localStorage.getItem("user"));

      if(userData ==null){
        
      }else{
        
        if (userData.tipo =="admin"){
          this.userType = true;
          this.router.navigateByUrl('/tabs/pedidos');
        }else{
          this.userType = false;
          this.router.navigateByUrl('/tabs/info');
        }

        
      }
    }

  // ------------------------ /



  // Borrado de cuenta //

  userDelete(){
  var user = firebase.auth().currentUser;

  user.delete().then(function(){
    
  }).catch(function(error){
  })

  this.userDeleteFromDatabase();

  }

  useraux:any
  userDeleteFromDatabase(){

    
  this.useraux=[];
  this.useraux = JSON.parse(localStorage.getItem("user"));
  let tot = this.useraux.id;


    this.db.collection('users').doc(tot).delete();
    localStorage.removeItem("categories");
    localStorage.removeItem("user");
    localStorage.removeItem("productos");
    localStorage.removeItem("ofertas");



    this.router.navigateByUrl("/home");
    this.presentToast("La cuenta ha sido eliminada con exito");
  }

  // ------------------- //


  // Editar datos de cuenta //

  userEdit(id:string,nombre:string,apellidos:string,telefono:string,correo:string,tipo:string,calle:string,localidad:string,numero:string,piso:string,puerta:string){

    this.useraux=[];
    this.useraux = JSON.parse(localStorage.getItem("user"));
    let tot = this.useraux.id;

    this.db.collection('users').doc(tot).delete();
  
      this.db.doc('/users/'+ id).set({
        id: id,
        nombre:nombre,
        apellidos:apellidos,
        correo:correo,
        tipo:tipo,
        direccion:{
          localidad:localidad,
          calle:calle,
          numero:numero,
          piso:piso,
          puerta:puerta
        },
        telefono:telefono,
      });
    
      localStorage.removeItem("categories");
      localStorage.removeItem("user");
      localStorage.removeItem("productos");
      localStorage.removeItem("carrito");
      
    
      setTimeout(()=>{
        this.router.navigateByUrl("/home");
        this.presentToast("Cambios realizados con exito");
      },2000);

      

  }
// -------------------------------------------------------- //

  carrito:any;
  usuario:any;

  pedido={
    id:"",
    usuario:"",
    pedido:"",
    calle:"",
    condicion:"",
    condicion2:"",
    estado:"1",
    dia:"",
    hora:"",
    tiempo:"",
    horanueva:"",
    observaciones:"",
    localidad:"",
    pago:"",
  }

  hora:any;
  minuto:any;
  segundo:any;
  fecha:any;
// procesado del pedido /

  realizarCompra(calle,condicion,condicion2,condicion3,horanueva,observaciones,localidad){

    this.loadingCtrl.create({
      message:'Procesando el pedido',
      spinner:'crescent'
    }) .then((loadingElement) =>{
      loadingElement.present();
    });
    

    this.carrito = JSON.parse(localStorage.getItem("carrito"));
    this.usuario = JSON.parse(localStorage.getItem("user"));
//---
    let d = new Date();

    this.fecha = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();

    this.hora = d.getHours();
    this.minuto= d.getMinutes();
    this.segundo = d.getSeconds();

//----
    this.pedido.id = this.db.createId();
    this.pedido.usuario = this.usuario;
    this.pedido.pedido = this.carrito;
    this.pedido.calle = calle;
    this.pedido.condicion = condicion;
    this.pedido.condicion2 = condicion2;
    this.pedido.horanueva = horanueva;
    this.pedido.observaciones = observaciones;
    this.pedido.localidad = localidad
    this.pedido.pago = condicion3;

    if(this.segundo < 10){
      this.segundo = "0" + this.segundo
    }

    if(this.minuto < 10 ){
      this.minuto = "0" + this.minuto;
    }

    this.pedido.dia = this.fecha;
    this.pedido.hora = this.hora + ":" + this.minuto + ":" + this.segundo;
    let dat = new Date().getTime();
    this.pedido.tiempo = dat.toString();
//---

    this.db.doc('/pedidos/'+ this.pedido.id).set(this.pedido);

    localStorage.removeItem("carrito");


    setTimeout(()=>{
      this.loadingCtrl.dismiss();
      this.printAlert();
      
    },3000);




  }

  // ----------------------- //


 async printAlert(){

  const alert = await this.alertcontroller.create({
    header: '¡Pedido Realizado!',
    message: 'El pedido ha sido realizado',
    buttons: [
    
       {
        text: 'Aceptar',
        handler: () => {
        }
      }

    ]
  });

  await alert.present();

  }

}
