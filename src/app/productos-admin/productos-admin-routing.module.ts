import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosAdminPage } from './productos-admin.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosAdminPageRoutingModule {}
