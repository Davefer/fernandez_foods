import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductosAdminPage } from './productos-admin.page';

describe('ProductosAdminPage', () => {
  let component: ProductosAdminPage;
  let fixture: ComponentFixture<ProductosAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductosAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
