import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosAdminPageRoutingModule } from './productos-admin-routing.module';

import { ProductosAdminPage } from './productos-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosAdminPageRoutingModule
  ],
  declarations: [ProductosAdminPage]
})
export class ProductosAdminPageModule {}
