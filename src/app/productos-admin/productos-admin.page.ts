import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AppserviceService } from '../services/appservice.service';



@Component({
  selector: 'app-productos-admin',
  templateUrl: './productos-admin.page.html',
  styleUrls: ['./productos-admin.page.scss'],
})
export class ProductosAdminPage implements OnInit {

  productos:any;

  constructor(
    private router:Router,
    private db:AngularFirestore, 
    private storage: AngularFireStorage,
    private alertcontroller:AlertController,
    private appservice:AppserviceService
  ) { }

  ngOnInit() {
    this.loadprods();
  }


  ionViewWillEnter(){
    this.loadprods();
  }


loadprods(){

  let categoriesCollection:AngularFirestoreCollection = this.db.collection("products");
  categoriesCollection.valueChanges().subscribe(

    res => {
      this.productos = [];

      res.forEach(element => {
        this.productos.push(element);
      });


      this.productos.sort(function(a, b) {
        if (a.nombre < b.nombre) {
          return -1;
        }
        if (a.nombre > b.nombre) {
          return 1;
        }
        return 0;
      });

    }
  )



}

  async borrarproducto(prod){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'El producto será eliminado. ¿Deseas Continuar?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            
          }
        }, {
          text: 'Aceptar',
          handler: () => {

            this.db.collection('products').doc(prod.id).delete()
            this.storage.storage.refFromURL(prod.imagen).delete();
        
            this.productos.splice(this.productos.indexOf(prod),1);

            this.alertafinal();

          }
        }
      ]
    });

    await alert.present();

  }


  async alertafinal(){

    const alert = await this.alertcontroller.create({
      header: '¡Producto eliminado!',
      message: 'El producto ha sido eliminado',
      buttons: [
         {
          text: 'Aceptar',
          handler: () => {
            console.log('eliminado');
          }
        }
      ]
    });
    await alert.present();
  }


  addnuevo(){
    this.router.navigateByUrl("/productos-admin-add");
  }

  editarproducto(prd){

    this.appservice.productID = prd;
    this.router.navigateByUrl("/productos-admin-edit")

  }


}
