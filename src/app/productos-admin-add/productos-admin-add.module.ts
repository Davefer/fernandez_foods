import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosAdminAddPageRoutingModule } from './productos-admin-add-routing.module';

import { ProductosAdminAddPage } from './productos-admin-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosAdminAddPageRoutingModule
  ],
  declarations: [ProductosAdminAddPage]
})
export class ProductosAdminAddPageModule {}
