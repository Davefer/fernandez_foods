import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosAdminAddPage } from './productos-admin-add.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosAdminAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosAdminAddPageRoutingModule {}
