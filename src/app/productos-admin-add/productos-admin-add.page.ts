import { Component, OnInit } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, createStorageRef } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { finalize, timeout } from 'rxjs/operators';

@Component({
  selector: 'app-productos-admin-add',
  templateUrl: './productos-admin-add.page.html',
  styleUrls: ['./productos-admin-add.page.scss'],
})
export class ProductosAdminAddPage implements OnInit {

  idnuevo:string;
  nombrenuevo:string;
  catnuevo:string;
  precionuevo:string;
  descnuevo:string;
  fb:any;
  base64image:any;
  auxnum:number = 0;

  options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };


  constructor(
    private camera:Camera,
    private alertcontroller:AlertController,
    private db:AngularFirestore,
    private storage:AngularFireStorage,
    private loadingCtrl:LoadingController,
    private router:Router,
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
  ) { }

    categorias:any

  ngOnInit() {


    this.idnuevo ="";
    this.nombrenuevo="";
    this.catnuevo="";
    this.precionuevo="";
    this.descnuevo="";
    this.base64image="";

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categorias = [];
        res.forEach(element => {
          this.categorias.push(element);
        });
      }

    )

  }


  ionViewWillEnter(){
  
    this.idnuevo ="";
    this.nombrenuevo="";
    this.catnuevo="";
    this.precionuevo="";
    this.descnuevo="";
    this.base64image="";

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categorias = [];
        res.forEach(element => {
          this.categorias.push(element);
        });
      }
    )
  }

  selectChange(selc){
    console.log(selc.detail.value);
    this.catnuevo = selc.detail.value;
  }





  subirImagen(){
   
    this.camera.getPicture(this.options).then( async (imageData) => {
      this.base64image = 'data:image/jpeg;base64,' + imageData;
      
  
     }, (err) => {
       console.log(err)
      // Handle error
     });
  }


  addNuevo(){
    console.log("creando");

    let idn = this.idnuevo.replace(/\s/g, '');
    let nmn = this.nombrenuevo.replace(/\s/g, '');
    let ctn = this.catnuevo.replace(/\s/g, '');
    let des = this.descnuevo.replace(/\s/g, '');

    if(idn.length == 0 || nmn.length == 0 || ctn.length == 0 || this.precionuevo.length == 0 || des.length == 0 || this.base64image==""){
      this.printalert("Necesitas introducir todos los datos");
    } else{

      let categoriesCollection:AngularFirestoreCollection = this.db.collection("products");
      categoriesCollection.valueChanges().subscribe(
  
        res => {
          this.auxnum = 0;
          res.forEach(element => {

            if(element.id == this.idnuevo){
              this.auxnum = 1;
              console.log("son iguales");
            } 
          });
          this.paso2();
        }
      )
    }

  }

paso2(){

  if(this.auxnum == 1){
    
    this.printalert("Ya existe un producto con esa ID");
  }else{

    this.loadingCtrl.create({
      message:'Añadiendo producto',
      spinner:'crescent'
    }) .then((loadingElement) =>{
      loadingElement.present();
    });

    let photoName = 'categorias/'+ this.idnuevo
    const fileRef = this.storage.ref(photoName);
    const task = fileRef.putString(this.base64image,'data_url');

    task.snapshotChanges().pipe(
      finalize(()=>{
        fileRef.getDownloadURL().subscribe(url =>{
          this.fb=url;
        });
      })).subscribe( look =>{
        
        setTimeout(() => {

          this.loadingCtrl.dismiss();
    
          this.db.doc('/products/' + this.idnuevo).set({
            id: this.idnuevo,
            nombre: this.nombrenuevo,
            categoria: this.catnuevo,
            descripcion:this.descnuevo,
            precio: parseFloat(this.precionuevo),
            imagen: this.fb
          }); 
    
          this.idnuevo ="";
          this.nombrenuevo="";
          this.catnuevo="";
          this.precionuevo="";
          this.descnuevo="";
          this.base64image="";
          this.fb="";
    
            this.router.navigateByUrl('/tabs/productosadmin')
            
          },3000);
  

      });


  }
}
 


 async printalert(msg){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: msg,
      buttons: [{
          text: 'Aceptar',
          handler: () => {
            
          }}]});
          await alert.present();
  }

  
  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/productosadmin');
  }


}
