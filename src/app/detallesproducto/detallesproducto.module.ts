import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallesproductoPageRoutingModule } from './detallesproducto-routing.module';

import { DetallesproductoPage } from './detallesproducto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetallesproductoPageRoutingModule
  ],
  declarations: [DetallesproductoPage]
})
export class DetallesproductoPageModule {}
