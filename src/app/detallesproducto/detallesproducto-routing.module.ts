import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallesproductoPage } from './detallesproducto.page';

const routes: Routes = [
  {
    path: '',
    component: DetallesproductoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallesproductoPageRoutingModule {}
