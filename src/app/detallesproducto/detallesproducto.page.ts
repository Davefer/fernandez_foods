import { Component, OnInit } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { AppserviceService } from '../services/appservice.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { TabsPage } from '../tabs/tabs.page';

@Component({
  selector: 'app-detallesproducto',
  templateUrl: './detallesproducto.page.html',
  styleUrls: ['./detallesproducto.page.scss'],
})
export class DetallesproductoPage {

  carritocount:any;
  prodData:any;
  cantidad:number;
  nombreprod:string;
  precioprod:number = 0;
  precio2:any;
  precio3:any;
  idprod:string;

  constructor(

    private appservice:AppserviceService ,
    private tabsp:TabsPage,
    private router:Router, 
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private db:AngularFirestore, 
  ) { }


 

  ionViewWillEnter(){


    this.cantidad = 0;

    let prodCollection:AngularFirestoreCollection = this.db.collection("products");
    prodCollection.valueChanges().subscribe(
  
      res => {
        this.prodData=[];

        res.forEach(element =>{

          if(element.id == this.appservice.productID){
            element.precio = parseFloat(element.precio)
            this.prodData.push(element);
            this.idprod = element.id;
            this.nombreprod = element.nombre;
            this.precioprod = element.precio;
            this.precio2 = element.precio.toString();
            console.log(this.precio2.length);

            
           }

        })
        console.log(this.prodData);

        if(this.precio2.length == 4){
          this.precio2 = this.precio2+"0";
        }

        if(this.precio2.length == 2 || this.precio2.length==1){
          this.precio2 = this.precio2+".00";
        }
        
        if(this.precio2.length == 3 ){
          this.precio2 = this.precio2+"0";
        }

      }
      
    )
    

  }



  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:400,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/cads');
  }


  // Contron Cantidades //

  rest(){
    if (this.cantidad > 0){
      this.cantidad = this.cantidad - 1;
    }
  }

  sum(){
    this.cantidad = this.cantidad + 1;
  }



  carrito:any;
  carritoaux:any;
  cant:number;

  prodaux ={
    id:"",
    nombre:"",
    cantidad:"",
    precio:""
  }


  alCarrito(){
  
    this.prodaux.id = this.idprod
    this.prodaux.nombre = this.nombreprod;
    this.prodaux.cantidad = this.cantidad.toString();
    let preciotot = this.precioprod * this.cantidad
    this.prodaux.precio = preciotot.toString();

    this.carritoaux = [];
    this.carrito = [];
    

    this.carrito = JSON.parse(localStorage.getItem("carrito"))
   
    if(this.cantidad > 0 ){
      

    if (this.carrito == "" || this.carrito == null){
      this.carritoaux.push(this.prodaux);
      localStorage.setItem("carrito",JSON.stringify(this.carritoaux));


    }else{
  
      this.carrito.forEach(element => {
        
        if(element.nombre == this.nombreprod){
          let cantidadprev = parseInt(element.cantidad);
          let cantidadnew = cantidadprev + this.cantidad;
          preciotot = cantidadnew * this.precioprod;
          this.carrito.splice(this.carrito.indexOf(element),1);
          this.prodaux.cantidad = cantidadnew.toString();
          this.prodaux.precio =preciotot.toString();
        }
      });
        this.carrito.push(this.prodaux);
        localStorage.setItem("carrito",JSON.stringify(this.carrito));
    }
    this.cantidad = 0;
    } else {
      this.appservice.presentToast("Selecciona una cantidad valida");
    }

    // contador cantidad productos carrito //
    this.carritocount = JSON.parse(localStorage.getItem("carrito"));
    this.tabsp.cantidades = 0;
    this.carritocount.forEach(element => {
      this.tabsp.cantidades = this.tabsp.cantidades + parseInt(element.cantidad);
    });
    // ....................................... //
  }
// ------------------------ //
}
