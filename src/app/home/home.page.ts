import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppserviceService } from '../services/appservice.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  correo:string = " ";
  passw:string = " ";

  correoc:string = "dark";
  passc:string = "dark";


  constructor(
    private router:Router,
    private appservice:AppserviceService,
  ) {}

  ngOnInit(){
    this.appservice.userSesion();
  }

  ionViewWillEnter(){
    this.correo = "";
    this.passw = "";
  }

  // Login //
  login(){
   
    let nm = this.correo.replace(/\s/g, '');
    let ps = this.passw.replace(/\s/g, '');

    if(nm.length == 0 || ps.length == 0){
     
        this.appservice.presentToast("Faltan campos por rellenar");

        if(nm.length == 0){
          this.correoc = "danger";
        }else{
          this.correoc = "dark";
        }

        if(nm.length == 0){
          this.passc = "danger";
        }else{
          this.passc = "dark";
        }

    }else {
      this.correoc = "dark";
      this.passc = "dark";

      this.appservice.userLogin(this.correo,this.passw);

      this.correo = "";
      this.passw = "";

    }
  }
// -------------- //


  // Registro //
  register(){
    this.router.navigateByUrl("/register");
  }
  // --------- //

}
