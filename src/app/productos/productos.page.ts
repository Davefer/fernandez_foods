import { Component, OnInit } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { AppserviceService } from '../services/appservice.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {
  
  productos:Array<string>;
  productos2:any;

  constructor(
    private appservice:AppserviceService ,
    private router:Router, 
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private db:AngularFirestore, 
    ) { }

  ngOnInit() {

              
  let categoriesCollection:AngularFirestoreCollection = this.db.collection("products");
  categoriesCollection.valueChanges().subscribe(

    res => {
      this.productos = [];
      this.productos2=[];
      let prodaux ={};
      res.forEach(element =>{
        prodaux = element;
        this.productos2.push(prodaux);
      })
      localStorage.setItem("productos",JSON.stringify(this.productos2));
      
      this.loadCat();

    }
  )

  }


  ionViewWillEnter(){
                  
  let categoriesCollection:AngularFirestoreCollection = this.db.collection("products");
  categoriesCollection.valueChanges().subscribe(

    res => {
      this.productos = [];
      this.productos2=[];
      let prodaux ={};
      res.forEach(element =>{
        prodaux = element;
        this.productos2.push(prodaux);
      })
      localStorage.setItem("productos",JSON.stringify(this.productos2));
      
      this.loadCat();

    }
  )
  }


  loadCat(){
    let loadaux  = JSON.parse(localStorage.getItem("productos"));
    loadaux.forEach(element => {
      if (element.categoria == this.appservice.categoryID){
        this.productos.push(element);
      }
    });

  }



  viewProduct(event){
    this.appservice.productID = event;
    this.router.navigateByUrl('tabs/details')
    
  };



// Animación back //
  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }
    
    this.appservice.categoryID = "";
    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/productos');
  }

// ------------------------ //
}
