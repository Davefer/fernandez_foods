import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { InformacionPage } from './informacion.page';
import { InformacionPageRoutingModule } from './informacion-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformacionPageRoutingModule,

  ],
  declarations: [InformacionPage]
})
export class InformacionPageModule {}
