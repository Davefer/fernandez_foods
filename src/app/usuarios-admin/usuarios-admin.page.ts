import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-usuarios-admin',
  templateUrl: './usuarios-admin.page.html',
  styleUrls: ['./usuarios-admin.page.scss'],
})
export class UsuariosAdminPage implements OnInit {


  usuarios:any;

  constructor(
    private db:AngularFirestore,
    private storage: AngularFireStorage,
    private alertcontroller:AlertController,
  ) { }

  ngOnInit() {
    let categoriesCollection:AngularFirestoreCollection = this.db.collection("users");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.usuarios = [];

        res.forEach(element => {
          this.usuarios.push(element);
        });

      }
    )
  }


  ionViewWillEnter(){
    let categoriesCollection:AngularFirestoreCollection = this.db.collection("users");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.usuarios = [];

        res.forEach(element => {
          this.usuarios.push(element);
        });

      }
    )
  }

  async borraruser(usr){
    
    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'El usuario será eliminado. ¿Deseas Continuar?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            
          }
        }, {
          text: 'Aceptar',
          handler: () => {

            this.db.collection('users').doc(usr.id).delete();
            this.usuarios.splice(this.usuarios.indexOf(usr),1);

            this.alertafinal();

          }
        }
      ]
    });

    await alert.present();
  }

  async alertafinal(){

    const alert = await this.alertcontroller.create({
      header: '¡Usuario Eliminado!',
      message: 'El usuario ha sido eliminado',
      buttons: [
         {
          text: 'Aceptar',
          handler: () => {

            console.log('eliminado');
          }
        }
      ]
    });
    await alert.present();
  }

}
