import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductosAdminEditPageRoutingModule } from './productos-admin-edit-routing.module';

import { ProductosAdminEditPage } from './productos-admin-edit.page';
import { ImagePicker } from '@ionic-native/image-picker/ngx';




@NgModule({
  providers:[
    ImagePicker
  ],

  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductosAdminEditPageRoutingModule,
  ],
  declarations: [ProductosAdminEditPage]

})
export class ProductosAdminEditPageModule {}
