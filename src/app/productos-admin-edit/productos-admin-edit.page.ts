import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { AppserviceService } from '../services/appservice.service';
import { finalize } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';


@Component({
  selector: 'app-productos-admin-edit',
  templateUrl: './productos-admin-edit.page.html',
  styleUrls: ['./productos-admin-edit.page.scss'],
})
export class ProductosAdminEditPage implements OnInit {

  constructor(
    private db:AngularFirestore,
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private alertcontroller:AlertController,
    private storage:AngularFireStorage,
    private router:Router,
    private appservice:AppserviceService,
    private loadingCtrl:LoadingController,
    private camera:Camera, 
  ) { }

  producto:any;
  categorias:any;

  nombrenuevo:string;
  valueselect:string;
  precionuevo:string;
  descnuevo:string;
  imgprod:string;
  idprod:string;

  auxnumber:number=0;
  fb:any;
  base64image:any;

  ngOnInit() {

  }


  ionViewWillEnter(){
    let prodCollection:AngularFirestoreCollection = this.db.collection("products");
    prodCollection.valueChanges().subscribe(

      res => {
        this.producto = [];
        res.forEach(element => {

          if(this.appservice.productID == element.id){

            if(element.precio.toString().length == 4){
              element.precio = element.precio+"0";
              
            }
      
            if(element.precio.toString().length == 2 || element.precio.toString().length == 1){
              element.precio = element.precio+".00";
              console.log(element.precio);
            }
            
            if(element.precio.toString().length == 3 ){
              element.precio =  element.precio+"0";
             
            }

            console.log("igual")
            this.producto.push(element);
            this.nombrenuevo = element.nombre;
            this.valueselect = element.categoria;
            this.precionuevo = element.precio;
            this.descnuevo = element.descripcion;
            this.imgprod = element.imagen;
            this.idprod = element.id;
          }

        });
      }

    )

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categorias = [];
        res.forEach(element => {
          this.categorias.push(element);
        });
      }

    )



  }


  editProducto(){

    this.loadingCtrl.create({
      message:'Editando el producto',
      spinner:'crescent'
    }) .then((loadingElement) =>{
      loadingElement.present();
    });
    
    if( this.nombrenuevo=="" || this.valueselect=="" || this.precionuevo=="" || this.descnuevo==""){
      
      this.printalert('Hay campos sin rellenar');

    } else {

      let np = this.nombrenuevo.replace(/\s/g, '');
      let cp = this.valueselect.replace(/\s/g, '');
      let pp = this.precionuevo.toString().replace(/\s/g,'');
      let desp = this.descnuevo.replace(/\s/g, '');


      if(np.length == 0 || cp.length == 0 || desp.length == 0 || pp.length == 0){

        this.printalert("Alguno de los datos no es valido");

      }else{


        if(this.auxnumber == 0){

            this.fb = this.imgprod;

        }else{

          let photoName = 'productos/'+ this.idprod
          const fileRef = this.storage.ref(photoName);
          const task = fileRef.putString(this.base64image,'data_url');
      
          task.snapshotChanges().pipe(
            finalize(()=>{
              fileRef.getDownloadURL().subscribe(url =>{
                this.fb=url;
              });
            })).subscribe();

        }



        setTimeout(()=>{

          console.log(this.precionuevo);
          console.log(parseFloat(this.precionuevo))

          this.loadingCtrl.dismiss();

          this.db.doc('/products/' + this.idprod).update({
            nombre:this.nombrenuevo,
            categoria:this.valueselect,
            precio:this.precionuevo, 
            descripcion:this.descnuevo,
            imagen : this.fb
          })

         this.printalert("Producto editado");
         this.router.navigateByUrl('/tabs/productosadmin')

        },3000)

      }

    }


  }


  options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };




subirImagen(){

  this.camera.getPicture(this.options).then( async (imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64 (DATA_URL):
    this.base64image = 'data:image/jpeg;base64,' + imageData;
    this.auxnumber = 1;
    

   }, (err) => {
     console.log(err)
    // Handle error
   });



}

selectChange(selc){
  console.log(selc.detail.value);
  this.valueselect = selc.detail.value;
}



  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/productosadmin');
  }




 async printalert(msg){

  const alert = await this.alertcontroller.create({
    header: '¡Alerta!',
    message: msg,
    buttons: [{
        text: 'Aceptar',
        handler: () => {

        }}]});

  await alert.present();

  }



}
