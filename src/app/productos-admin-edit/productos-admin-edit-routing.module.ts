import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductosAdminEditPage } from './productos-admin-edit.page';

const routes: Routes = [
  {
    path: '',
    component: ProductosAdminEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosAdminEditPageRoutingModule {}
