import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriasAdminPageRoutingModule } from './categorias-admin-routing.module';

import { CategoriasAdminPage } from './categorias-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriasAdminPageRoutingModule
  ],
  declarations: [CategoriasAdminPage]
})
export class CategoriasAdminPageModule {}
