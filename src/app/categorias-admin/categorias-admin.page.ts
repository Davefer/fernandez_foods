import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categorias-admin',
  templateUrl: './categorias-admin.page.html',
  styleUrls: ['./categorias-admin.page.scss'],
})
export class CategoriasAdminPage implements OnInit {

  categorias:any;

  constructor(
    private db:AngularFirestore, 
    private storage: AngularFireStorage,
    private alertcontroller:AlertController,
    private appservice:AppserviceService,
    private router:Router,
  ) { }

  ngOnInit() {    

  }

  ionViewWillEnter(){
    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categorias = [];

        res.forEach(element => {
          this.categorias.push(element);
        });

        this.categorias.sort(function(a, b) {
          if (a.nombre < b.nombre) {
            return -1;
          }
          if (a.nombre > b.nombre) {
            return 1;
          }
          return 0;
        });

      }
    )
  }

  async borrarcategoria(cat){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'La categoría será eliminada. ¿Deseas Continuar?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            
          }
        }, {
          text: 'Aceptar',
          handler: () => {

            this.db.collection('categories').doc(cat.id).delete()
            this.storage.storage.refFromURL(cat.imagen).delete();
            this.categorias.splice(this.categorias.indexOf(cat),1);
            this.alertafinal();

          }
        }
      ]
    });
    await alert.present();
  }

  editarCategoria(catid){

  this.appservice.categoryID = catid;
  this.router.navigateByUrl("/categorias-admin-edit");

  }

  addnuevo(){
    this.router.navigateByUrl("/categorias-admin-add");
  }


  async alertafinal(){

    const alert = await this.alertcontroller.create({
      header: '¡Categoría eliminado!',
      message: 'La categoría ha sido eliminada',
      buttons: [
         {
          text: 'Aceptar',
          handler: () => {

            console.log('eliminado');
          }
        }
      ]
    });
    await alert.present();
  }


}
