import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriasAdminPage } from './categorias-admin.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriasAdminPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriasAdminPageRoutingModule {}
