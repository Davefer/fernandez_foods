import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { CategoriasPage } from './categorias.page';
import { CategoriasPageRoutingModule } from './categorias-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriasPageRoutingModule
   
  ],
  declarations: [CategoriasPage]
})
export class CategoriasPageModule {}
