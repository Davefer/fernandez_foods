import { Component, OnInit } from '@angular/core';
import { AppserviceService } from '../services/appservice.service';
import { Router } from '@angular/router';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.page.html',
  styleUrls: ['./categorias.page.scss'],
})
export class CategoriasPage implements OnInit {

  categories:Array<string>;
  categories2:any;

  constructor(
    private appservice:AppserviceService,
    private router:Router,
    private db:AngularFirestore, 
  ) {}

  ngOnInit() {

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categories = [];
        this.categories2=[];
        let cataux ={};
        res.forEach(element =>{
          cataux = element;
          this.categories2.push(cataux);
        })
        localStorage.setItem("categories",JSON.stringify(this.categories2));
        this.categories = JSON.parse(localStorage.getItem("categories"));

      }
    )

  }

  ionViewWillEnter(){

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categories = [];
        this.categories2=[];
        let cataux ={};
        res.forEach(element =>{
          cataux = element;
          this.categories2.push(cataux);
        })
        localStorage.setItem("categories",JSON.stringify(this.categories2));
        this.categories = JSON.parse(localStorage.getItem("categories"));

      }
    )

  }

  gotoProducts(event){
    this.appservice.categoryID = event;
    this.router.navigateByUrl("/tabs/productoslist");
  }

}
