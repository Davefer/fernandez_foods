import { Component, OnInit } from '@angular/core';
import { TabsPage } from '../tabs/tabs.page';
import { AlertController, ModalController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';
import { CarritoModalPage } from '../carrito-modal/carrito-modal.page';


@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.page.html',
  styleUrls: ['./carrito.page.scss'],
})
export class CarritoPage implements OnInit {

  constructor(
   private tabsp:TabsPage,
   private alertcontroller:AlertController,
   private modalCtrl:ModalController,
   private appservice:AppserviceService,
  ) { }

  carrito:any
  carritos:any
  totalprecio:number=0;

  ngOnInit() {
  }


  ionViewWillEnter(){
    this.totalprecio;
    this.carrito=[];
    this.carritos=[];
    this.carrito = JSON.parse(localStorage.getItem("carrito"));


    this.carritos = JSON.parse(localStorage.getItem("carrito"));

    this.calculartotal()
  }

  
  quitar(idpro){

  this.carritos.forEach(element => {

    if(element.id == idpro){

      // resta cantidad del producto en el budge //

      this.tabsp.cantidades = this.tabsp.cantidades - element.cantidad;

      //..........................................//

      this.carritos.splice(this.carritos.indexOf(element),1)
      this.carrito = this.carritos;
      
      localStorage.setItem("carrito",JSON.stringify(this.carritos));
      console.log(this.carritos);

      this.calculartotal()

      if(this.carritos.length == 0){
        localStorage.removeItem("carrito");
        this.calculartotal()
      }
    }
  });
  }

totalaux:number=0;
aux2:number = 0;

  calculartotal(){
    
    this.totalprecio = 0;
    this.carritos.forEach(element => {

      console.log(element.precio);

      this.totalprecio += parseFloat(element.precio)

    });
  }

  
  compra:any;

async  realizarCompra(){

    this.compra = JSON.parse(localStorage.getItem("carrito"));
    console.log(this.compra);

    if(this.compra == "" || this.compra ==null){

        this.appservice.presentToast("El carrito está vacío")

    } else {
      
      let date1 = new Date().getDay()

      if(date1 != 1){
        
        let date2  = new Date().getHours();

        if (date2 <15 ){
      
          const modal = await this.modalCtrl.create({
            component:CarritoModalPage,

          });

          modal.onWillDismiss().then(()=>{
            this.carrito = JSON.parse(localStorage.getItem("carrito"));
            this.carritos = JSON.parse(localStorage.getItem("carrito"));
            this.tabsp.cantidades = 0;
            this.calculartotal();
          })
 
          this.carrito =[];
          this.carritos = [];
          this.totalprecio;

        return await modal.present();

        }else{
          this.printAlert();
        }
      }else {
        this.printAlert();
      }
    }



  }



  async printAlert(){
    const alert = await this.alertcontroller.create({
      header: '¡Servicio no Disponible!',
      message: 'Nuestro servicio no está disponible actualmente, consulte nuestros horarios en el apartado de Información para saber mas',
      buttons: [
      
         {
          text: 'Aceptar',
          handler: () => {
            
          }
        }

      ]
    });
  
    await alert.present();
  }


}
