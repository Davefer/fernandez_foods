import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarritoPageRoutingModule } from './carrito-routing.module';

import { CarritoPage } from './carrito.page';
import { CarritoModalPage } from '../carrito-modal/carrito-modal.page';
import { CarritoModalPageModule } from '../carrito-modal/carrito-modal.module';

@NgModule({
  entryComponents:[CarritoModalPage],

  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarritoPageRoutingModule,
    CarritoModalPageModule
  ],
  declarations: [CarritoPage]
})
export class CarritoPageModule {}
