import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NavController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  
  namec:string = "dark";
  apellidoc:string = "dark";
  tlfc:string = "dark";
  mailc:string = "dark";
  passc:string = "dark";

  nombre:string="";
  apellidos:string="";
  correo:string="";
  telefono:string="";
  passw:string="";
  passwrepeat:string="";

  constructor(
    private appservice:AppserviceService ,
    private router:Router, 
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController) { }

  ngOnInit() {
  }


  // vuelta a pagina principal

  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/home');
  }

// ----------------------------------- //



// Creación de nuevo usuario //
  newUser = {
    id:"",
    nombre:"",
    apellidos:"",
    correo:"",
    telefono:"",
    tipo:"normal",
  }

  doReg(){

    if(this.passw == this.passwrepeat){


      
    let nm = this.nombre.replace(/\s/g, '');
    let ap = this.apellidos.replace(/\s/g, '');
    let cr = this.correo.replace(/\s/g, '');
    let pw = this.passw.replace(/\s/g, '');
    let tlf = this.telefono;

    if (nm.length == 0 || ap.length == 0 || cr.length == 0 || pw.length==0 || tlf.length < 9 ) {
      this.appservice.presentToast("faltan campos por rellenar");

        if(nm.length == 0){
          this.namec ="danger";
        }else{
          this.namec ="dark";
        }

        if(ap.length == 0){
          this.apellidoc ="danger";
        }else{
          this.apellidoc ="dark";
        }

        if(cr.length == 0){
          this.mailc ="danger";
        }else{
          this.mailc ="dark";
        }

        if(pw.length == 0){
          this.passc ="danger";
        }else{
          this.passc ="dark";
        }

        if(tlf.length < 9){
          this.tlfc = "danger";
        }else{
          this.tlfc = "dark";
        }

    }else{
       this.newUser.nombre = this.nombre.trim();
       this.newUser.apellidos = this.apellidos.trim();
       this.newUser.correo = this.correo.trim();
       this.newUser.telefono = this.telefono;
       pw = this.passw.trim();



       this.appservice.userCreate(this.newUser,pw);
       
       this.nombre="";
       this.apellidos="";
       this.correo="";
       this.passw="";
       this.passwrepeat="";
       this.telefono="";
    }


    } else {
      this.appservice.presentToast("Las contraseñas no coinciden");
    }


  
  }
// ------------------------------------------------------ //




}
