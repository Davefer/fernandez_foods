import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path:'',
    redirectTo:'productos'

  },

  {
    path:'productoslist',
    redirectTo:'cads'

  },
  

  {
    path: '',
    component: TabsPage,
    children:[
      {
        path:'productos',
        loadChildren:() => import("./../categorias/categorias.module").then(m => m.CategoriasPageModule)
      },

      {
        path:'info',
        loadChildren:() => import("./../informacion/informacion.module").then(m => m.InformacionPageModule)
      },

      {
        path:'descuentos',
        loadChildren:() => import("./../descuentos/descuentos.module").then(m => m.DescuentosPageModule)
      },

      {
        path:'carrito',
        loadChildren:() => import("./../carrito/carrito.module").then(m => m.CarritoPageModule)
      },

      {
        path:'cuenta',
        loadChildren:() => import("./../cuenta/cuenta.module").then(m => m.CuentaPageModule)
      },

      {
        path:'cads',
        loadChildren:() => import("./../productos/productos.module").then(m => m.ProductosPageModule)
      },

      {
        path:'details',
        loadChildren:() => import("./../detallesproducto/detallesproducto.module").then(m => m.DetallesproductoPageModule)
      },

      {
        path:'editardatos',
        loadChildren:() => import("./../editardatos/editardatos.module").then(m => m.EditardatosPageModule)
      },

      {
        path:'pedidos',
        loadChildren:() => import("./../pedidos/pedidos.module").then(m => m.PedidosPageModule)
      },
      
      {
        path:'detallespedido',
        loadChildren:() => import("./../detallespedido/detallespedido.module").then(m => m.DetallespedidoPageModule)
      },

      {
        path:'productosadmin',
        loadChildren:() => import("./../productos-admin/productos-admin.module").then(m => m.ProductosAdminPageModule)
      },

      {
        path:'categoriasadmin',
        loadChildren:() => import("./../categorias-admin/categorias-admin.module").then(m => m.CategoriasAdminPageModule)
      },

      {
        path:'ofertasadmin',
        loadChildren:() => import("./../descuentos-admin/descuentos-admin.module").then(m => m.DescuentosAdminPageModule)
      },

      {
        path:'usuariosadmin',
        loadChildren:() => import("./../usuarios-admin/usuarios-admin.module").then(m => m.UsuariosAdminPageModule)
      },

    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
