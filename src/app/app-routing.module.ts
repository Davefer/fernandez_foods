import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'categorias',
    loadChildren: () => import('./categorias/categorias.module').then( m => m.CategoriasPageModule)
  },
  {
    path: 'informacion',
    loadChildren: () => import('./informacion/informacion.module').then( m => m.InformacionPageModule)
  },
  {
    path: 'descuentos',
    loadChildren: () => import('./descuentos/descuentos.module').then( m => m.DescuentosPageModule)
  },
  {
    path: 'carrito',
    loadChildren: () => import('./carrito/carrito.module').then( m => m.CarritoPageModule)
  },
  {
    path: 'cuenta',
    loadChildren: () => import('./cuenta/cuenta.module').then( m => m.CuentaPageModule)
  },
  {
    path: 'productos',
    loadChildren: () => import('./productos/productos.module').then( m => m.ProductosPageModule)
  },
  {
    path: 'detallesproducto',
    loadChildren: () => import('./detallesproducto/detallesproducto.module').then( m => m.DetallesproductoPageModule)
  },
  {
    path: 'editardatos',
    loadChildren: () => import('./editardatos/editardatos.module').then( m => m.EditardatosPageModule)
  },
  {
    path: 'carrito-modal',
    loadChildren: () => import('./carrito-modal/carrito-modal.module').then( m => m.CarritoModalPageModule)
  },
  {
    path: 'pedidos',
    loadChildren: () => import('./pedidos/pedidos.module').then( m => m.PedidosPageModule)
  },
  {
    path: 'detallespedido',
    loadChildren: () => import('./detallespedido/detallespedido.module').then( m => m.DetallespedidoPageModule)
  },
  {
    path: 'productos-admin',
    loadChildren: () => import('./productos-admin/productos-admin.module').then( m => m.ProductosAdminPageModule)
  },
  {
    path: 'categorias-admin',
    loadChildren: () => import('./categorias-admin/categorias-admin.module').then( m => m.CategoriasAdminPageModule)
  },
  {
    path: 'descuentos-admin',
    loadChildren: () => import('./descuentos-admin/descuentos-admin.module').then( m => m.DescuentosAdminPageModule)
  },
  {
    path: 'usuarios-admin',
    loadChildren: () => import('./usuarios-admin/usuarios-admin.module').then( m => m.UsuariosAdminPageModule)
  },
  {
    path: 'productos-admin-add',
    loadChildren: () => import('./productos-admin-add/productos-admin-add.module').then( m => m.ProductosAdminAddPageModule)
  },
  {
    path: 'productos-admin-edit',
    loadChildren: () => import('./productos-admin-edit/productos-admin-edit.module').then( m => m.ProductosAdminEditPageModule)
  },
  {
    path: 'categorias-admin-add',
    loadChildren: () => import('./categorias-admin-add/categorias-admin-add.module').then( m => m.CategoriasAdminAddPageModule)
  },
  {
    path: 'categorias-admin-edit',
    loadChildren: () => import('./categorias-admin-edit/categorias-admin-edit.module').then( m => m.CategoriasAdminEditPageModule)
  },
  {
    path: 'descuentos-admin-add',
    loadChildren: () => import('./descuentos-admin-add/descuentos-admin-add.module').then( m => m.DescuentosAdminAddPageModule)
  },
  {
    path: 'descuentos-admin-edit',
    loadChildren: () => import('./descuentos-admin-edit/descuentos-admin-edit.module').then( m => m.DescuentosAdminEditPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
