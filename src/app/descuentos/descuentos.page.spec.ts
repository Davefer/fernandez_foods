import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescuentosPage } from './descuentos.page';

describe('DescuentosPage', () => {
  let component: DescuentosPage;
  let fixture: ComponentFixture<DescuentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescuentosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescuentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
