import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { TabsPage } from '../tabs/tabs.page';



@Component({
  selector: 'app-descuentos',
  templateUrl: './descuentos.page.html',
  styleUrls: ['./descuentos.page.scss'],
})
export class DescuentosPage implements OnInit {

  constructor(
    private db:AngularFirestore, 
    private tabsp:TabsPage,
  ) { }

  ofertasData:any;
  ofertasCarrito:any
  ofertasCarritoAux:any;


  ngOnInit() {}

  ionViewWillEnter() {
    
    let prodCollection:AngularFirestoreCollection = this.db.collection("ofertas");
    prodCollection.valueChanges().subscribe(
      res => {
        this.ofertasData=[];
        res.forEach(element =>{
            if(element.precio.toString().length == 4 ){
              element.precio = element.precio+"0"
            }
            if(element.precio.toString().length == 2 || element.precio.toString().length == 1){
              element.precio = element.precio+".00";
            }
            if(element.precio.toString().length== 3){
              element.precio = element.precio+"0";
            }
            this.ofertasData.push(element);
        })
        console.log(this.ofertasData);
        localStorage.setItem("ofertas",JSON.stringify(this.ofertasData));
      }
      
    )

  }
  
  oferta = {
    id:"",
    nombre:"",
    cantidad: 1 ,
    precio:""

  }

  cant:any;
  precio:any;

  addDescuento(item){

    this.oferta = {id:"",nombre:"",cantidad:1,precio:""};

    this.ofertasCarrito = [];
    this.ofertasCarritoAux = [];
    this.cant = 0;
    this.precio = 0;

    this.ofertasCarrito = JSON.parse(localStorage.getItem("carrito"));

    this.oferta.id = item.id;
    this.oferta.nombre = item.nombre;
    this.oferta.precio = item.precio;

    if (this.ofertasCarrito == "" || this.ofertasCarrito == null){

      this.ofertasCarritoAux.push(this.oferta);
      localStorage.setItem("carrito",JSON.stringify(this.ofertasCarritoAux));
      this.tabsp.cantidades = this.tabsp.cantidades + 1;

    } else{
      this.ofertasCarrito.forEach(element => {
        if(element.nombre == item.nombre){
          let cant = element.cantidad + 1;
          console.log(cant)
          let precio = item.precio * cant
          console.log(precio)
          this.oferta.cantidad = cant;
          this.oferta.precio = precio.toString();
          this.ofertasCarrito.splice(this.ofertasCarrito.indexOf(element),1);
        }

      });

      this.ofertasCarrito.push(this.oferta);
      localStorage.setItem("carrito",JSON.stringify(this.ofertasCarrito));
      this.tabsp.cantidades = this.tabsp.cantidades + 1;
    }  
  }

}
