import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriasAdminAddPage } from './categorias-admin-add.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriasAdminAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriasAdminAddPageRoutingModule {}
