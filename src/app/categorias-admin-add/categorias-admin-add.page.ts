import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize, timeout } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-categorias-admin-add',
  templateUrl: './categorias-admin-add.page.html',
  styleUrls: ['./categorias-admin-add.page.scss'],
})
export class CategoriasAdminAddPage implements OnInit {

  idnuevo:string;
  nombrenuevo:string;

  fb:any;
  base64image:any;
  auxnum:number = 0;
 

  options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };


  constructor(
    private camera:Camera,
    private alertcontroller:AlertController,
    private db:AngularFirestore,
    private storage:AngularFireStorage,
    private loadingCtrl:LoadingController,
    private router:Router,
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
  ) { }

  ngOnInit() {
    this.idnuevo ="";
    this.nombrenuevo="";
    this.fb="";
    this.base64image="";
  }

  ionViewWillEnter(){
    this.idnuevo ="";
    this.nombrenuevo="";
    this.fb="";
    this.base64image="";    
  }


  subirImagen(){

    this.camera.getPicture(this.options).then( async (imageData) => {
      this.base64image = 'data:image/jpeg;base64,' + imageData;
      
     }, (err) => {
       console.log(err)
      // Handle error
     });

  }

  addNuevo(){
    let nm = this.idnuevo.replace(/\s/g, '');
    let idm = this.nombrenuevo.replace(/\s/g, '');

    if(nm.length==0 || idm.length==0 || this.base64image.length==0 ){
      this.printalert("Necesitas introducir todos los datos");
    } else{
      
      let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
      categoriesCollection.valueChanges().subscribe(
  
        res => {
          this.auxnum = 0;
          res.forEach(element => {

            if(element.id == this.idnuevo){
              this.auxnum = 1;
              console.log("son iguales");
            } 
          });
          this.paso2();
        }
      )
    }

  }
  
paso2(){
  
  if(this.auxnum == 1){

    this.printalert("Ya existe una categoría con esa ID")
  } else {

    this.loadingCtrl.create({
      message:'Añadiendo categoría',
      spinner:'crescent'
    }) .then((loadingElement) =>{
      loadingElement.present();
    });

    let photoName = 'categorias/'+ this.idnuevo
    const fileRef = this.storage.ref(photoName);
    const task = fileRef.putString(this.base64image,'data_url');

    task.snapshotChanges().pipe(
      finalize(()=>{
        fileRef.getDownloadURL().subscribe(url =>{
          this.fb=url;
        });
      })).subscribe( look => {

        setTimeout(() => {

          this.loadingCtrl.dismiss();
    
          this.db.doc('/categories/' + this.idnuevo).set({
            id: this.idnuevo,
            nombre: this.nombrenuevo,
            imagen: this.fb
          }); 
    
          this.idnuevo ="";
          this.nombrenuevo="";
          this.base64image="";
          this.fb="";
    
            this.router.navigateByUrl('/tabs/categoriasadmin')
          
          },4000);
  

      });
  }
}

  async printalert(msg){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: msg,
      buttons: [{
          text: 'Aceptar',
          handler: () => {
  
          }}]});
  
    await alert.present();
  
    }

    back(){

      let options : NativeTransitionOptions = {
        direction: 'right',
        duration:200,
        slowdownfactor:-1,
        iosdelay:50
      }
  
      this.pageTransition.slide(options);
      this.navCtrl.navigateRoot('/tabs/categoriasadmin');
    }


}
