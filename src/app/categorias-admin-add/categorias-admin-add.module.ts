import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriasAdminAddPageRoutingModule } from './categorias-admin-add-routing.module';

import { CategoriasAdminAddPage } from './categorias-admin-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriasAdminAddPageRoutingModule
  ],
  declarations: [CategoriasAdminAddPage]
})
export class CategoriasAdminAddPageModule {}
