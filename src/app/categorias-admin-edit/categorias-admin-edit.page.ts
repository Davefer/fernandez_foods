import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController, LoadingController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { AppserviceService } from '../services/appservice.service';
import { finalize } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-categorias-admin-edit',
  templateUrl: './categorias-admin-edit.page.html',
  styleUrls: ['./categorias-admin-edit.page.scss'],
})
export class CategoriasAdminEditPage implements OnInit {

  constructor(
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private db:AngularFirestore,
    private appservice:AppserviceService,
    private camera:Camera,
    private alertcontroller:AlertController,
    private loadingCtrl:LoadingController,
    private storage:AngularFireStorage,
    private router:Router,
  ) { }

  categorias:any;
  idcat:any;
  nombrecat:string;
  imgcat:any;
  base64image:any;
  imgnew:any;
  auxnumber:number = 0;

  ngOnInit() {
  }

  ionViewWillEnter(){

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("categories");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.categorias = [];
        res.forEach(element => {

          if(this.appservice.categoryID == element.id){

            this.idcat = element.id;
            this.nombrecat = element.nombre;
            this.imgcat = element.imagen;
          }

        
        });
      }

    )

  }


  editProducto(){

    this.loadingCtrl.create({
      message:'Editando Categoría',
      spinner:'crescent'
    }) .then((loadingElement) =>{
      loadingElement.present();
    });

if(this.nombrecat ==""){
  this.printalert("Hay campos sin rellenar")
} else {

  let nm = this.nombrecat.replace(/\s/g, '');

  if(nm.length == 0) {
    this.printalert("Hay campos sin rellenar");
  } else {
    if (this.auxnumber == 0){
      this.imgnew = this.imgcat
    } else{



      let photoName = 'categorias/'+ this.idcat
      const fileRef = this.storage.ref(photoName);
      const task = fileRef.putString(this.base64image,'data_url');
  
      task.snapshotChanges().pipe(
        finalize(()=>{
          fileRef.getDownloadURL().subscribe(url =>{
            this.imgnew=url;
          });
        })).subscribe();

    }

    setTimeout(()=>{

      this.loadingCtrl.dismiss();

      this.db.doc('/categories/' + this.idcat).update({
        nombre:this.nombrecat,
        imagen : this.imgnew
      })

     this.printalert("Categoría editada");
     this.router.navigateByUrl('/tabs/categoriasadmin')

    },3000)


  }

}

}


  options: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  };

  subirImagen(){

    this.camera.getPicture(this.options).then( async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64image = 'data:image/jpeg;base64,' + imageData;
      this.auxnumber = 1;

     }, (err) => {
       console.log(err)
      // Handle error
     });

  }

  async printalert(msg){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: msg,
      buttons: [{
          text: 'Aceptar',
          handler: () => {
  
          }}]});
  
    await alert.present();
  
    }


  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/categoriasadmin');
  }
}
