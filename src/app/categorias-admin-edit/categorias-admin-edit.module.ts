import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriasAdminEditPageRoutingModule } from './categorias-admin-edit-routing.module';

import { CategoriasAdminEditPage } from './categorias-admin-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriasAdminEditPageRoutingModule
  ],
  declarations: [CategoriasAdminEditPage]
})
export class CategoriasAdminEditPageModule {}
