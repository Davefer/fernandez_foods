import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriasAdminEditPage } from './categorias-admin-edit.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriasAdminEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriasAdminEditPageRoutingModule {}
