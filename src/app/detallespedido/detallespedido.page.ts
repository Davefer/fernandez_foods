import { Component, OnInit } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AppserviceService } from '../services/appservice.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { TabsPage } from '../tabs/tabs.page';


@Component({
  selector: 'app-detallespedido',
  templateUrl: './detallespedido.page.html',
  styleUrls: ['./detallespedido.page.scss'],
})
export class DetallespedidoPage implements OnInit {

  pedidoID:string;
  pedidoData:any;
  hiderealizado:boolean=false;

  constructor(
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private db:AngularFirestore, 
    private appservice:AppserviceService,
    private callnumber:CallNumber,
    private alertcontroller:AlertController,
    private tabsp:TabsPage,
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.pedidoID = this.appservice.pedidoID;
    this.pedidoData = [];

    let categoriesCollection:AngularFirestoreCollection = this.db.collection("pedidos");
    categoriesCollection.valueChanges().subscribe(
  
      res => {
        this.pedidoData = [];
        res.forEach(element => {
          
          if(element.id == this.pedidoID){
            this.pedidoData.push(element);
            console.log(this.pedidoData);

            if(element.estado == "2"){
              this.hiderealizado = true;
            } else {
              this.hiderealizado = false;
            }

          }

        });

        console.log(this.pedidoData);

      }
    )

  }


  llamarCliente(numero){
    console.log(numero);

    this.callnumber.callNumber(numero, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));

  }

  marcarRealizado(pedido){
    console.log(pedido);

    this.db.collection("pedidos").doc(pedido.id).update({
      "estado": "2",


  });

  this.tabsp.pedidos = this.tabsp.pedidos - 1;

  this.printAlert();

  }


  async printAlert(){
    const alert = await this.alertcontroller.create({
      header: '¡Pedido Completado!',
      message: 'El pedido se ha movido a pedidos Completados',
      buttons: [
      
         {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.navigateRoot('/tabs/pedidos');
          }
        }

      ]
    });
  
    await alert.present();
  }


  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/pedidos');
  }

}
