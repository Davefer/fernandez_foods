import { Component, OnInit } from '@angular/core';
import { AppserviceService } from '../services/appservice.service';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { TabsPage } from '../tabs/tabs.page';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  pedidos:any;
  pedidosaux:any
  pedidosSi:any;
  pedidosNo:any;
  listaActual:string="no"


  constructor(
    private appservice:AppserviceService,
    private router:Router,
    private db:AngularFirestore, 
    private tabsp:TabsPage
  ) { }

  ngOnInit() {

  }


  ionViewWillEnter(){            
  let categoriesCollection:AngularFirestoreCollection = this.db.collection("pedidos");
  categoriesCollection.valueChanges().subscribe(

    res => {
      this.tabsp.pedidos = 0;
      this.pedidos = [];
      this.pedidosaux = [];
      this.pedidosNo = [];
      this.pedidosSi = [];

      res.forEach(element => {
        this.pedidosaux.push(element);

      });

      this.pedidosaux.sort(function(a, b) {
        if (a.tiempo > b.tiempo) {
          return -1;
        }
        if (a.tiempo < b.tiempo) {
          return 1;
        }
        return 0;
      });

  
      this.pedidosaux.forEach(element => {
        
        if(element.estado == 1){
          this.pedidosNo.push(element);
          this.tabsp.pedidos = this.tabsp.pedidos + 1;
        }else{
          this.pedidosSi.push(element);
        }

        if(this.listaActual == "no"){
          this.pedidos = this.pedidosNo

        }else{
          this.pedidos = this.pedidosSi

        }

      });
    }
  )
  }

// Ver detalles del pedido //

  verPedido(idpedido){

    this.appservice.pedidoID = idpedido;
    this.router.navigateByUrl('tabs/detallespedido');

  }
// ---------- //


segmentChanged(segmento){

 this.listaActual = segmento.detail.value

  if(this.listaActual == "no"){
    this.pedidos = this.pedidosNo
  }else{
    this.pedidos = this.pedidosSi

}

}

}
