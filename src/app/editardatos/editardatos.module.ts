import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditardatosPageRoutingModule } from './editardatos-routing.module';

import { EditardatosPage } from './editardatos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditardatosPageRoutingModule
  ],
  declarations: [EditardatosPage]
})
export class EditardatosPageModule {}
