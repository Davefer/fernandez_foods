import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { NavController, AlertController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';

@Component({
  selector: 'app-editardatos',
  templateUrl: './editardatos.page.html',
  styleUrls: ['./editardatos.page.scss'],
})
export class EditardatosPage implements OnInit {

  constructor(
    private router:Router,
    private pageTransition:NativePageTransitions,
    public navCtrl:NavController,
    private appservice:AppserviceService,
    private alertcontroller:AlertController,
  ) { }

  id:string;
  nombre:string;
  apellidos:string;
  telefono:string;
  correo:string;
  tipo:string;

  calle:string;
  localidad:string;
  numero:string;
  piso:string;
  puerta:string;

  ngOnInit() {

  }

  ionViewWillEnter(){
    let storage = JSON.parse(localStorage.getItem("user"));
    this.id = storage.id;
    this.nombre = storage.nombre;
    this.apellidos = storage.apellidos;
    this.telefono = storage.telefono;
    this.correo = storage.correo;
    this.tipo = storage.tipo;
    this.calle = storage.direccion.calle;
    this.localidad = storage.direccion.localidad;
    this.numero = storage.direccion.numero;
    this.piso = storage.direccion.piso;
    this.puerta =storage.direccion.puerta;
  }


  back(){

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:200,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/tabs/cuenta');

  }


 async doChanges(){

  let nm = this.nombre.replace(/\s/g, '');
  let ap = this.apellidos.replace(/\s/g, '');
  let cor = this.correo.replace(/\s/g, '');
  let tlf  = this.telefono.replace(/\s/g, '');
  let dir = this.calle.replace(/\s/g, '');
  let loc = this.localidad.replace(/\s/g, '');
  

  if(nm.length == 0 || ap.length == 0 || cor.length == 0 || tlf.length == 0 || dir.length == 0 || loc.length == 0 || this.numero.length == 0 || this.piso.length == 0 || this.puerta.length == 0 ){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'Faltan campos por rellenar',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {

          }
        }
      ]
    });
    await alert.present();
  } else {
    this.appservice.userEdit(this.id,this.nombre,this.apellidos,this.telefono,this.correo,this.tipo,this.calle,this.localidad,this.numero,this.piso,this.puerta);
  }

}




}
