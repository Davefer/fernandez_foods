import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditardatosPage } from './editardatos.page';

const routes: Routes = [
  {
    path: '',
    component: EditardatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditardatosPageRoutingModule {}
