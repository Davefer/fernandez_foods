import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditardatosPage } from './editardatos.page';

describe('EditardatosPage', () => {
  let component: EditardatosPage;
  let fixture: ComponentFixture<EditardatosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditardatosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditardatosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
