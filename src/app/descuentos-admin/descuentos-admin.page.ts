import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AppserviceService } from '../services/appservice.service';

@Component({
  selector: 'app-descuentos-admin',
  templateUrl: './descuentos-admin.page.html',
  styleUrls: ['./descuentos-admin.page.scss'],
})
export class DescuentosAdminPage implements OnInit {

  ofertas:any;

  constructor(
    private db:AngularFirestore,
    private storage: AngularFireStorage,
    private alertcontroller:AlertController,
    private router:Router,
    private appservice:AppserviceService,
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.ofertas=[]
    let categoriesCollection:AngularFirestoreCollection = this.db.collection("ofertas");
    categoriesCollection.valueChanges().subscribe(

      res => {
        this.ofertas = [];

        res.forEach(element => {

          this.ofertas.push(element);

        });

        this.ofertas.sort(function(a, b) {
          if (a.nombre < b.nombre) {
            return -1;
          }
          if (a.nombre > b.nombre) {
            return 1;
          }
          return 0;
        });

      }
    )
  }


  async borraroferta(ofe){

    const alert = await this.alertcontroller.create({
      header: '¡Alerta!',
      message: 'La Oferta será eliminada. ¿Deseas Continuar?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            
          }
        }, {
          text: 'Aceptar',
          handler: () => {

            this.db.collection('ofertas').doc(ofe.id).delete()
            this.ofertas.splice(this.ofertas.indexOf(ofe),1);
            this.alertafinal();

          }
        }
      ]
    });

    await alert.present();

  }

  editaroferta(ofe2){
    this.appservice.descuentoID = ofe2;
    this.router.navigateByUrl('/descuentos-admin-edit');
  }


  async alertafinal(){

    const alert = await this.alertcontroller.create({
      header: '¡Oferta eliminada!',
      message: 'La oferta ha sido eliminada',
      buttons: [
         {
          text: 'Aceptar',
          handler: () => {

            console.log('eliminado');
          }
        }
      ]
    });
    await alert.present();
  }

  addnuevo(){
    this.router.navigateByUrl("/descuentos-admin-add");
  }

}
