import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescuentosAdminPageRoutingModule } from './descuentos-admin-routing.module';

import { DescuentosAdminPage } from './descuentos-admin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescuentosAdminPageRoutingModule
  ],
  declarations: [DescuentosAdminPage]
})
export class DescuentosAdminPageModule {}
