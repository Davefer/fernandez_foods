import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescuentosAdminPage } from './descuentos-admin.page';

describe('DescuentosAdminPage', () => {
  let component: DescuentosAdminPage;
  let fixture: ComponentFixture<DescuentosAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescuentosAdminPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescuentosAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
