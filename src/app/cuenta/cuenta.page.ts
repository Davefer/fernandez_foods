import { Component, OnInit } from '@angular/core';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppserviceService } from '../services/appservice.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.page.html',
  styleUrls: ['./cuenta.page.scss'],
})
export class CuentaPage implements OnInit {

  constructor(
    private pageTransition:NativePageTransitions,
    private router:Router,
    public navCtrl:NavController,
    private appservice:AppserviceService,
    private alertcontroller:AlertController,
  ) { }

  userData:any;
  userId:string;
  userNombre:string;
  userApellidos:string;
  userCorreo:string;
  userLocalidad:string;
  userCalle:string;
  userTelefono:string;
  
  ngOnInit() {
    
  }

  

  ionViewWillEnter(){
    this.userData = [];
    this.userData = JSON.parse(localStorage.getItem("user"));

    this.userId = this.userData.id;
    this.userNombre = this.userData.nombre;
    this.userApellidos = this.userData.apellidos;
    this.userCorreo = this.userData.correo;
    this.userTelefono = this.userData.telefono;
    this.userCalle = this.userData.direccion.calle + " " + this.userData.direccion.numero+"-"+this.userData.direccion.piso+"-"+this.userData.direccion.puerta;
    this.userLocalidad = this.userData.direccion.localidad;
  }


  // Cerrar Sesion //
  cerrarSesion(){
    localStorage.removeItem("user");
    localStorage.removeItem("productos");
    localStorage.removeItem("categories");
    localStorage.removeItem("carrito");
    localStorage.removeItem("ofertas");

    let options : NativeTransitionOptions = {
      direction: 'right',
      duration:400,
      slowdownfactor:-1,
      iosdelay:50
    }

    this.pageTransition.slide(options);
    this.navCtrl.navigateRoot('/home');

  }
// ------------------------- //


cambiarDatos(){
  
  this.router.navigateByUrl("/tabs/editardatos");

}


async borrarCuenta(){
  
  const alert = await this.alertcontroller.create({
    header: '¡Alerta!',
    message: '¿Estas segur@ que quieres eliminar tu cuenta ?',
    buttons: [
      {
        text: 'Cancelar',
        handler: () => {
          
        }
      }, {
        text: 'Aceptar',
        handler: () => {
          this.appservice.userDelete();
        }
      }
    ]
  });

  await alert.present();


  
}

}
